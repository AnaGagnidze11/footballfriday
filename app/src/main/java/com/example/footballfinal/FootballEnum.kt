package com.example.footballfinal

enum class FootballEnum (val type: Int){
    GOAL(1),
    YELLOW_CARD(2),
    RED_CARD(3),
    SUBSTITUTION(4)
}

enum class GoalType(val type: Int){
    GOAL(1),
    OWN_GOAL(2)
}

enum class MatchTeamType(val type: Int){
    TEAM1(1),
    TEAM2(2)

}