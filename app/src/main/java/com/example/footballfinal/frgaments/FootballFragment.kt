package com.example.footballfinal.frgaments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.footballfinal.R
import com.example.footballfinal.databinding.FragmentFootballBinding

class FootballFragment : Fragment() {

    private var _binding: FragmentFootballBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (_binding == null){
            _binding = FragmentFootballBinding.inflate(inflater, container, false)
        }
        return binding.root
    }



    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}