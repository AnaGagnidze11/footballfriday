package com.example.footballfinal.models

data class Football(val resultCode: Int, val match: Match)

data class Match(val matchDate: Long?,
                 val matchSummary: MatchSummary?,
                 val matchTime: Double?,
                 val stadiumAdress: String?,
                 val team1: Team?,
                 val team2: Team?
)

data class MatchSummary(val summaries: List<Summary>?)

data class Team(val ballPosition: Int?,
                val score: Int?,
                val teamImage: String?,
                val teamName: String?)


data class Summary(
    val actionTime: String?,
    val team1Action: List<TeamAction>?,
    val team2Action: List<TeamAction>?
)

data class TeamAction(
    val action: Action?,
    val actionType: Int?,
    val teamType: Int?
)

data class Action(
    val goalType: Int?,
    val player: Player?,
    val player1: Player?,
    val player2: Player?
)
data class Player(
    val playerImage: String? = null,
    val playerName: String?
)