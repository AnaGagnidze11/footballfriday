package com.example.footballfinal.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.footballfinal.models.Football
import com.example.footballfinal.network.ResultControl
import com.example.footballfinal.network.RetrofitService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FootballViewModel : ViewModel() {

    private val infoLiveData = MutableLiveData<ResultControl<Football>>().apply {
        mutableListOf<Football>()
    }

    val _infoLiveData: LiveData<ResultControl<Football>> = infoLiveData



    fun init() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                getFootballInfo()
            }
        }
    }


    private suspend fun getFootballInfo() {
        infoLiveData.postValue(ResultControl.loading(true))
        val result = RetrofitService.retrofitService.getMatchDetails()
        if (result.isSuccessful) {
            val info = result.body()
            info?.let {
                infoLiveData.postValue(ResultControl.success(info))
            }
        }else{
            infoLiveData.postValue(ResultControl.error(result.errorBody().toString()))
        }
    }
}